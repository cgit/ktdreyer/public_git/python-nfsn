%if 0%{?fedora}
%global with_python3 1
%endif

%define srcname python-nfsn

Name:           python-nfsn
Version:        1.1.1
Release:        1%{?dist}
Summary:        Interact with NearlyFreeSpeech's API
License:        CC0
URL:            https://github.com/ktdreyer/python-nfsn
Source0:        http://pypi.python.org/packages/source/p/%{srcname}/%{srcname}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  pytest
BuildRequires:  python2-devel
BuildRequires:  python-beanbag
BuildRequires:  python-httpretty
BuildRequires:  python-requests
%if 0%{?with_python3}
BuildRequires:  python3-pytest
BuildRequires:  python3-devel
BuildRequires:  python3-beanbag
BuildRequires:  python3-httpretty
BuildRequires:  python3-requests
%endif # with_python3

%description
A modern Python library for accessing NearlyFreeSpeech.net's API.

%package -n python2-nfsn
Summary:        %{summary}
Requires:       python2-beanbag
%{?python_provide:%python_provide python2-nfsn}

%description -n python2-nfsn
A modern Python library for accessing NearlyFreeSpeech.net's API.

%if 0%{?with_python3}
%package -n python3-nfsn
Summary:        %{summary}
Requires:       python3
Requires:       python3-beanbag

%description -n python3-nfsn
A modern Python library for accessing NearlyFreeSpeech.net's API.
%endif # with_python3

%prep
%autosetup -n %{srcname}-%{version}

%build
%{py2_build}

%if 0%{?with_python3}
%{py3_build}
%endif # with_python3

%install
%py2_install

%if 0%{?with_python3}
rm %{buildroot}%{_bindir}/pynfsn
%py3_install
%endif # with_python3

%check
%{__python2} setup.py test

%if 0%{?with_python3}
%{__python3} setup.py test
%endif # with_python3

%files -n python2-nfsn
%doc README.rst
%license COPYING
%{python2_sitelib}/*
%if ! 0%{?with_python3}
%{_bindir}/pynfsn
%endif

%if 0%{?with_python3}
%files -n python3-nfsn
%doc README.rst
%license COPYING
%{python3_sitelib}/*
%{_bindir}/pynfsn
%endif # with_python3

%changelog
* Sat Dec 12 2015 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.1.1-1
- initial package
